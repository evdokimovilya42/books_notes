from django.db import models


class BookLanguage(models.Model):
    slug = models.SlugField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Book(models.Model):
    all_pages = models.IntegerField()
    read_pages = models.IntegerField()

    def __str__(self):
        if self.book_names.all():
            return self.book_names.all().first().name
        return str(self.pk)

    def get_name(self):
        return self.book_names.all().get(language__slug="ru").name

    def get_review(self):
        return self.book_reviews.all().get(language__slug="ru").review

    def get_reading_status(self):
        if self.read_pages == self.all_pages:
            return "прочитал полностью"
        return "не осилил"

    def get_versions_count(self):
        return BookLog.objects.filter(book=self).count()


class BookName(models.Model):
    name = models.CharField(max_length=255)
    book = models.ForeignKey(Book, related_name="book_names", on_delete=models.CASCADE)
    language = models.ForeignKey(BookLanguage, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class BookReview(models.Model):
    review = models.TextField()
    book = models.ForeignKey(Book, related_name="book_reviews", on_delete=models.CASCADE)
    language = models.ForeignKey(BookLanguage, on_delete=models.CASCADE)


class BookLog(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book_logs")
    name = models.CharField(max_length=255,  null=True)
    language = models.ForeignKey(BookLanguage, on_delete=models.CASCADE, null=True)
    review = models.TextField(null=True)
    all_pages = models.IntegerField(null=True)
    read_pages = models.IntegerField(null=True)
    datetime = models.DateTimeField(auto_now=True)
