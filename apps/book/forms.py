from django import forms
from apps.book.models import Book


class BookAdminForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'

    def clean_read_pages(self):
        all_pages = self.cleaned_data['all_pages']
        read_pages = self.cleaned_data['read_pages']
        if read_pages > all_pages:
            raise forms.ValidationError("Number of pages you'v read more then number of all pages in book")
        return read_pages