from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import BookName, BookReview, Book, BookLog


@receiver(pre_save, sender=Book)
def book_save_handler(sender, instance, **kwargs):
    tracked_fields = ["read_pages", "all_pages"]
    if Book.objects.filter(pk=instance.pk):
        old_instance = Book.objects.get(pk=instance.pk)
        new_instance = instance
        old_instance_values = [old_instance.__dict__.get(field) for field in tracked_fields]
        new_instance_values = [new_instance.__dict__.get(field) for field in tracked_fields]
        if old_instance_values != new_instance_values:
            BookLog.objects.create(book=old_instance, all_pages=old_instance.all_pages,
                                   read_pages=old_instance.read_pages)


@receiver(pre_save, sender=BookName)
def book_name_save_handler(sender, instance, **kwargs):
    if BookName.objects.filter(pk=instance.pk):
        old_instance = BookName.objects.get(pk=instance.pk)
        new_instance = instance
        if old_instance.name != new_instance.name:
            BookLog.objects.create(book=old_instance.book,name=old_instance.name, language=new_instance.language)


@receiver(pre_save, sender=BookReview)
def book_review_save_handler(sender, instance, **kwargs):
    if BookReview.objects.filter(pk=instance.pk):
        old_instance = BookReview.objects.get(pk=instance.pk)
        new_instance = instance
        if old_instance.review != new_instance.review:
            BookLog.objects.create(book=old_instance.book, review=old_instance.review, language=new_instance.language)
