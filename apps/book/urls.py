from django.urls import path
from apps.book import views

urlpatterns = [
    path('', views.BookList.as_view(), name="book_list"),
    path('books/<int:pk>', views.BookDetailView.as_view(), name="book_detail")
]
