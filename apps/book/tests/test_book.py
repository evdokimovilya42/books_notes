from apps.common.tests.test_common import CommonTestFactory
from apps.book.models import BookLog, BookName, BookReview
from apps.book.forms import BookAdminForm


class BookTestFactory(CommonTestFactory):

    def test_get_book_name(self):
        book_ru_name = self.book.book_names.get(language__slug="ru").name
        name = self.book.get_name()
        self.assertEqual(name, book_ru_name)

    def test_get_book_reviews(self):
        book_ru_review = self.book.book_reviews.get(language__slug="ru").review
        review = self.book.get_review()
        self.assertEqual(review, book_ru_review)

    def test_read_pages_validation(self):
        new_read_pages = self.book.all_pages + 100
        form = BookAdminForm(data={"pk": self.book.pk, "all_pages": self.book.all_pages,
                                   "read_pages": new_read_pages})
        form.is_valid()
        self.assertEqual(form.errors["read_pages"][0], "Number of pages you'v read more then number "
                                                       "of all pages in book")

    def test_get_book_version(self):
        self.book.read_pages = 15
        self.book.save()
        self.book.refresh_from_db()
        book_versions = self.book.get_versions_count()
        self.assertEqual(book_versions, 1)

    def test_book_log_base_info(self):
        new_all_pages = self.test_all_pages + 1
        new_read_pages = self.test_read_pages + 1
        self.book.all_pages = new_all_pages
        self.book.read_pages = new_read_pages
        self.book.save()
        log = BookLog.objects.get(book=self.book)
        self.assertEqual(log.read_pages, self.test_read_pages)
        self.assertEqual(log.all_pages, self.test_all_pages)
        self.book.read_pages += 1
        self.book.all_pages += 1
        self.book.save()
        log = BookLog.objects.filter(book=self.book).last()
        self.assertEqual(log.read_pages, new_read_pages)
        self.assertEqual(log.all_pages, new_all_pages)

    def test_book_log_name(self):
        new_name = self.test_book_name + "test"
        old_name_instance = self.book.book_names.get(language__slug="ru")
        old_name = old_name_instance.name
        old_name_instance.name = new_name
        old_name_instance.save()
        log = BookLog.objects.filter(book=self.book).last()
        self.assertEqual(log.name, old_name)
        self.assertEqual(log.language.slug, "ru")

    def test_book_log_review(self):
        new_review = self.test_book_review + "test"
        old_review_instance = self.book.book_reviews.get(language__slug="ru")
        old_review = old_review_instance.review
        old_review_instance.review = new_review
        old_review_instance.save()
        log = BookLog.objects.filter(book=self.book).last()
        self.assertEqual(log.review, old_review)
        self.assertEqual(log.language.slug, "ru")
