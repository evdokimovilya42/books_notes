from django.contrib import admin
from apps.book.models import Book, BookName, BookReview, BookLanguage, BookLog
from apps.book.forms import BookAdminForm


class BookLogAdmin(admin.TabularInline):
    readonly_fields = ("name", "review", "all_pages", "read_pages", "language", "datetime")
    ordering = ["-datetime"]
    model = BookLog


class BookNameAdmin(admin.TabularInline):
    model = BookName
    max_num = 4


class BookReviewAdmin(admin.TabularInline):
    model = BookReview
    max_num = 4


class BookAdmin(admin.ModelAdmin):
    form = BookAdminForm
    inlines = [BookNameAdmin, BookReviewAdmin, BookLogAdmin]


class BookLanguageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Book, BookAdmin)
admin.site.register(BookLanguage, BookLanguageAdmin)