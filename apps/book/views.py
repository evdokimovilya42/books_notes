from django.views import generic
from apps.book.models import Book


class BookList(generic.ListView):
    model = Book
    template_name = "index.html"


class BookDetailView(generic.DetailView):
    model = Book
    template_name = "book_detail.html"
    lang = "ru"

    def get_context_data(self, **kwargs):
        lang_param = self.request.GET.get("lang")
        if lang_param:
            self.lang = lang_param
        name = self.object.book_names.filter(language__slug=self.lang)
        review = self.object.book_reviews.filter(language__slug=self.lang)
        if name and review:
            kwargs.update({"book_name": name.first(), "book_review": review.first()})
        return kwargs
