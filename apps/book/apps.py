from django.apps import AppConfig


class BookConfig(AppConfig):
    label = "book"
    name = "apps.book"

    def ready(self):
        from apps.book import signals