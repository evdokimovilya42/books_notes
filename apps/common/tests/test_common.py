from django.test import TestCase
from apps.book.models import Book, BookName, BookReview, BookLanguage
from django.test import Client
from django.contrib.auth.models import User


class CommonTestFactory(TestCase):

    def setUp(self):
        self.test_book_name = "Автостопом по галлкатике"
        self.test_book_review = "Хорошая книга"
        self.test_all_pages = 42
        self.test_read_pages = 12
        self.language = self.create_language()
        self.book = self.create_book()
        self.create_book_description_other_language()
        self.client = Client()
        self.test_user_name = "test_user"
        self.test_user_password = "12345678A"
        self.test_user_email = "test@ytest.ru"
        self.user = self.create_user(self.test_user_name, self.test_user_password)

    def create_language(self):
        return BookLanguage.objects.create(slug="ru", name="russian")

    def create_book(self):
        book = Book.objects.create(all_pages=self.test_all_pages, read_pages=self.test_read_pages)
        BookName.objects.create(name=self.test_book_name, language=self.language, book=book)
        BookReview.objects.create(review=self.test_book_review, language=self.language, book=book)
        return book

    def create_book_description_other_language(self):
        en_language = BookLanguage.objects.create(slug="en", name="english")
        BookName.objects.create(name="The Hitchhiker's Guide to the Galaxy", language=en_language, book=self.book)
        BookReview.objects.create(review="Interesting book", language=en_language, book=self.book)

    def create_user(self, name, password):
        user = User.objects.create(username=name, is_superuser=True, email=self.test_user_email, is_staff=True)
        user.set_password(self.test_user_password)
        user.save()
        return user

