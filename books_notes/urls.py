from django.contrib import admin
from django.urls import path, include
from apps.book.urls import urlpatterns as book_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(book_urls)),
]
